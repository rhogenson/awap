from __future__ import division
import networkx as nx
import random
from base_player import BasePlayer
from settings import *
import math

# Checks if we can use a given path
def path_is_valid(state, path):
    graph = state.get_graph()
    for i in range(0, len(path) - 1):
        if graph.edge[path[i]][path[i + 1]]['in_use']:
            return False
    return True

def pick_path(state, paths):
    """Return a path that is valid from a list of paths"""
    for path in paths:
        if path_is_valid(state, path):
            return path
    return None

class OrderFreqs():
    """Store the frequencies of each of the orders"""
    def __init__(self):
        self.freqs = {}
        self.curr_orders = set()

    def get_freq(self, node):
        """Return how many times this node has had an order"""
        if node in self.freqs:
            return self.freqs[node]
        return 0

    def update_orders(self, orders):
        """Update the frequencies with a list of new orders"""
        orders = set(orders)
        new_orders = orders - self.curr_orders
        self.curr_orders = orders
        for order in new_orders:
            o_node = order.get_node()
            if o_node in self.freqs:
                self.freqs[o_node] += 1
            else:
                self.freqs[o_node] = 1

def find_start(graph):
    """Find the best starting location"""
    n_nodes = graph.order()
    width = int(math.sqrt(n_nodes))
    m_node_idx = width // 2 * width + width // 2
    m_node = graph.nodes()[m_node_idx]
    best_node = None
    best_node_score = 0
    for node in graph:
        p = len(nx.shortest_path(graph, node, m_node)) - 1
        if p <= width / 4:
            if graph.degree(node) + p / n_nodes > best_node_score:
                best_node_score = graph.degree(node) + p / n_nodes
                best_node = node
    return best_node
    
def find_optimal_station(graph, stations, freqs):
    """Find good location for station midgame"""
    intMaxFreq = 0
    nodeStart = None
    for node in graph:
        if freqs.get_freq(node) > intMaxFreq:
            isGood = True
            for node_s in stations:
                if (len(nx.shortest_path(graph, node, node_s))
                        <= int(math.sqrt(graph.order())/2.5)):
                    isGood = False
            if isGood:
                intMaxFreq = freqs.get_freq(node)
                nodeStart = node
    return nodeStart

def best_paths(state, stations, orders):
    """Return pairs of order, path for the best path to each order"""
    graph = state.get_graph()
    orders.sort(key=lambda order: money_for_order(stations, order, state))
    for order in orders:
        print(stations)
        path = pick_path(state, list_of_paths(graph, stations, order))
        if path != None:
            money = (order.get_money() - DECAY_FACTOR * len(path) -
                DECAY_FACTOR * (state.get_time() - order.get_time_created()))
            if money > 0:
                yield (order, path)

def sorted_paths(graph, station, order_node):
    """Return a list of all paths from station to order_node, but sorted"""
    shortest_length = len(nx.shortest_path(graph, station, order_node))
    all_paths = list(filter(lambda s: len(s) < shortest_length + 5,
            nx.all_shortest_paths(graph, station, order_node)))
    all_paths.sort(key=len)
    return (path for path in all_paths)

def list_of_paths(graph, stations, order):
    """Return sorted list of paths from a station to the order"""
    paths = merge_paths([sorted_paths(graph, node, order.get_node())
            for node in stations])
    return paths

def merge_paths(paths):
    """Return a generator containing sorted paths"""
    first_paths = [next(p) for p in paths]

    while len(first_paths) > 0:
        pathlen = 2000
        optimalpath = None
        p_index = 0
        for i in range(len(first_paths)):
            p = first_paths[i]
            if len(p) < pathlen:
                optimalpath = p
                pathlen = len(p)
                p_index = i
        yield optimalpath
        try:
            first_paths[p_index] = next(paths[p_index])
        except StopIteration:
            first_paths.pop(p_index)
            paths.pop(p_index)

def money_for_order(stations, order, state):
    """Return the order maximizing money gained"""
    graph = state.get_graph()
    path = pick_path(state, list_of_paths(graph, stations, order))
    if path is None:
        return 0
    length = len(path)
    money = (order.get_money() - DECAY_FACTOR * len(path) -
        DECAY_FACTOR * (state.get_time() - order.get_time_created()))
    return money

class Player(BasePlayer):
    """
    You will implement this class for the competition. DO NOT change the class
    name or the base class.
    """
    
    def __init__(self, state):
        """
        Initializes your Player. You can set up persistent state, do analysis
        on the input graph, engage in whatever pre-computation you need. This
        function must take less than Settings.INIT_TIMEOUT seconds.
        --- Parameters ---
        state : State
            The initial state of the game. See state.py for more information.
        """
        self.stations = []
        self.order_freqs = OrderFreqs()

        return

    def build_stations(self, state, commands):
        """Build stations if it is wise to do so"""
        graph = state.get_graph()
        # Build an initial station
        if state.get_time() == 0:
            best_node = find_start(graph)
            commands.append(self.build_command(best_node))
            self.stations.append(best_node)
        else:
            cost_of_station = (INIT_BUILD_COST *
                    BUILD_FACTOR ** len(self.stations))
            if (state.get_money() >= cost_of_station and
                    state.get_time() <= GAME_LENGTH and
                    len(self.stations) < HUBS and
                    state.get_time() > graph.order()):
                best_place = find_optimal_station(graph, self.stations,
                        self.order_freqs)
                if best_place is not None:
                    commands.append(self.build_command(best_place))
                    self.stations.append(best_place)

    def fulfill_orders(self, state, commands):
        """Push commands to fulfill the most optimal order"""
        graph = state.get_graph()
        pending_orders = state.get_pending_orders()
        self.order_freqs.update_orders(pending_orders)
        if len(pending_orders) != 0:
            #order = best_order(graph, self.stations, pending_orders, state)

            paths = best_paths(state, self.stations, pending_orders)
            for (order, path) in paths:
                commands.append(self.send_command(order, path))

    def step(self, state):
        """
        Determine actions based on the current state of the city. Called every
        time step. This function must take less than Settings.STEP_TIMEOUT
        seconds.
        --- Parameters ---
        state : State
            The state of the game. See state.py for more information.
        --- Returns ---
        commands : dict list
            Each command should be generated via self.send_command or
            self.build_command. The commands are evaluated in order.
        """
        commands = []

        self.build_stations(state, commands)
        self.fulfill_orders(state, commands)
        return commands

# vim: set softtabstop=4 shiftwidth=4 expandtab :
